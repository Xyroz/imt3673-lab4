package com.example.lordxyroz.lab4;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    final private FirebaseFirestore db = FirebaseFirestore.getInstance();
    final private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    final private DateFormat dateFormat = new SimpleDateFormat("dd MMM yyy 'at' HH:mm:ss");

    private TextToSpeech tts = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(mAuth.getCurrentUser() == null) {

            mAuth.signInAnonymously()
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                Log.d("debug", "signInAnonymously:success");
                            } else {
                                Log.w("error", "signInAnonymously:failure");
                                Toast.makeText(MainActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                    });

        }

        checkNickname();

        Button btn = findViewById(R.id.send_button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText text = findViewById(R.id.input);
                CollectionReference reference = db.collection("messages");
                Map<String, Object> message = new HashMap<>();

                message.put("message", text.getText().toString());
                message.put("user", getNickname());
                message.put("time", dateFormat.format(Calendar.getInstance().getTime()));

                reference.add(message);

                text.setText("");
            }
        });

        final ListView view = findViewById(R.id.list_of_messages);

        CollectionReference reference = db.collection("messages");

        final List<String> messages = new ArrayList<>();

        reference.orderBy("time")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                messages.add(document.get("time") + "\n"  + document.get("user") + ": " + document.get("message"));
                            }

                            ArrayAdapter adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, messages);
                            view.setAdapter(adapter);
                            view.setSelection(messages.size() - 1);

                        }
                    }
                });



    }

    @Override
    protected void onStop() {
        super.onStop();
        if (tts != null) tts.shutdown();
    }

    public void speak(final String str) {
        if (tts != null) {
            tts.shutdown();
        }
        tts = new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i == TextToSpeech.SUCCESS) {
                    tts.setLanguage(Locale.JAPANESE);
                    tts.speak(str, TextToSpeech.QUEUE_FLUSH, null, null);
                }
            }
        });
    }

    private void setupNickname() {
        Log.d("debug", "setupNickname:no nickname");

        final Dialog dialog = new Dialog(MainActivity.this);

        dialog.setTitle("Welcome to the chat group!");

        dialog.setContentView(R.layout.nickname_popup);
        dialog.setCancelable(false);


        Button btn = dialog.findViewById(R.id.nickname_ok);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final EditText text = dialog.findViewById(R.id.nickname_text);
                final String nickname = text.getText().toString();

                if (nickname.length() > 0 && nickname.length() < 36) {

                    final CollectionReference reference = db.collection("users");

                    reference.get()
                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                    if (task.isSuccessful()) {

                                        boolean found = false;

                                        for (QueryDocumentSnapshot document : task.getResult()) {

                                            String name = document.get("name").toString();
                                            if (name.equals(nickname)) found = true;

                                            if (!found) {
                                                Map<String, Object> user = new HashMap<>();
                                                user.put("name", nickname);
                                                user.put("uid", mAuth.getCurrentUser().toString());

                                                reference.add(user);

                                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                                                SharedPreferences.Editor editor = prefs.edit();

                                                editor.putString("nickname", nickname);
                                                editor.apply();

                                                String display = "Welcome, " + nickname + "!";
                                                speak(display);
                                                Toast.makeText(MainActivity.this, display, Toast.LENGTH_SHORT).show();

                                                dialog.dismiss();
                                            }
                                            else {
                                                text.setError("Nickname already found!");
                                            }
                                        }
                                    }
                                }
                            });
                }
                else {
                    text.setError("Between 1 and 35 characters!");
                }
            }
        });
        dialog.show();
    }

    private void checkNickname() {
        final String nickname = getNickname();
        if (nickname == "") setupNickname();
        else {
            speak("Welcome back, " + nickname);
            Toast.makeText(MainActivity.this, "Welcome back, " + nickname, Toast.LENGTH_SHORT);
        }
    }

    private String getNickname() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        return prefs.getString("nickname", "");
    }
}
